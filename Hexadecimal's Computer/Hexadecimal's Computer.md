## Description
Pada suatu pagi di bulan november, sesuatu yang buruk terjadi: Seorang hacker menyerang
komputer hexadesimal yang digunakan sebagai server carbon, ia memasukan virus yang 
berisi sebanyak n angka natural mulai dari 1 sampai n untuk mengambil alih server tersebut

Tapi sayangnya rencananya gagal karena alasan yang simple: komputer hexadesimal tidak
menerima informasi selain angka yang ditulis dalam format binary. Maksutnya, jika sebuah
angka dalam bentuk desimal memiliki karakter selain angka 0 dan 1, angka tersebut tidak
tersimpan di dalam memory. Sekarang hacker tersebut ingin tahu, berapa banyak angka
yang benar benar masuk ke dalam komputer hexadesimal.

## Input
Input data berupa satu angka n (1 <= n <= 10^9).

## Output
banyaknya angka yang masuk ke dalam memory

## Contoh Input 
```
10
```

## Contoh Output
```
2
```

### Note
Untuk n = 10 terdapat 2 angka yang tersimpan, yaitu 1 dan 10