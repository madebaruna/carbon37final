import java.util.Scanner;

public class HexadesimalComputer {
  static int c = 0, n;
  
  public static void func(int x){
    if(x <= n){
      c++;
    }else {
      return;
    }
    func(10*x + 0);
    func(10*x + 1);
  }

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    n = sc.nextInt();
    func(1);
    System.out.println(c);
    
  }
}
