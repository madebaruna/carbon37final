## Deskripsi
Pada suatu jalan terdapat beberapa gedung yang berderet dan memiliki tinggi yang beragam.
Pak Bos tidak punya kerjaan dan ingin mencari persegi atau persegi panjang terbesar yang terbentuk
dari gedung-gedung tersebut. lebar dari masing-masing gedung adalah 1.

![alt text](https://bccjudgement-filkom.ub.ac.id/uploads/contoh1.png)  
dari gambar diatas ukuran yang terbesar adalah 12

![alt text](https://bccjudgement-filkom.ub.ac.id/uploads/contoh2.png)  
dari gambar diatas ukuran yang terbesar adalah 14

## Format Input
Baris pertama adalah jumlah gedung X  
X integer selanjutnya adalah tinggi masing-masing gedung

## Contoh Input
```
12
1 3 6 3 2 2 3 3 1 0 3 6
```

## Contoh Output
```
14
```

## Contoh Input
```
11
1 2 3 4 2 3 5 2 1 0 8
```

## Contoh Output
```
14
```

## Contoh Input
```
7
5 1 1 1 1 1 0
```

## Contoh Output
```
6
```


## Batasan
- $$ 0 <= X < 100000 $$