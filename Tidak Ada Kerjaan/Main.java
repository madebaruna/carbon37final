import java.util.*;
import java.io.*;

class Main {
  static Stack<Integer> hStack = new Stack<Integer>();
  static Stack<Integer> posStack = new Stack<Integer>();
  static int h, pos;
  static int tempH, tempPos;
  static int maxSize = -9999999;
  static int tempSize = 0;

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int jumlah = in.nextInt();
    int hist[] = new int[jumlah];
    for(int i=0; i<jumlah; i++) {
      hist[i] = in.nextInt();
    }

    for (pos = 0; pos < hist.length; pos++) {
      h = hist[pos];
      if (hStack.size() == 0 || h > hStack.get(hStack.size() - 1)) {
        hStack.push(h);
        posStack.push(pos);
      } else if (h < hStack.get(hStack.size() - 1)) {
        while (hStack.size() > 0 && h < hStack.get(hStack.size() - 1)) {
          pop();
        }
        hStack.push(h);
        posStack.push(tempPos);
      }
    }
    while (hStack.size() > 0) {
      pop();
    }

    System.out.println(maxSize);
  }

  public static void pop() {
    tempH = hStack.pop();
    tempPos = posStack.pop();
    tempSize = tempH * (pos - tempPos);
    maxSize = Math.max(tempSize, maxSize);
  }
}