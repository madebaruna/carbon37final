import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(), k = in.nextInt(), i, j, m = 0, count = 0, arr[] = new int[k];
        
        for (i = 0; i < n; i++) arr[in.nextInt() % k]++;
        if (k % 2 == 0 && arr[(int) Math.floor(k / 2)] > 0) count++;
        if (arr[0] > 0) count++;
        for (i = 1; i < k / 2.0; i++) if (i != k - i) count += Math.max(arr[i], arr[k - i]);
        
        System.out.println(count);
    }
}