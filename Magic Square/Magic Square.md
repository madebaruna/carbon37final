## Problem
Didefinisikan sebuah <a href="https://en.wikipedia.org/wiki/Magic_square" target="_blank">Magic Square</a> sebagai sebuah matrix `n x n` yang terdiri dari integer positif yang berbeda-beda dari 1 hingga n<sup>2</sup> dimana jumlah dari setiap baris, kolom, maupun diagonal selalu bernilai sama.

Anggaplah terdapat matrix `S` `3 x 3` yang terdiri dari integer [1, 9], kita bisa mengonversi setiap digit, a, ke digit lainnya, b, pada interval [1, 9] dengan biaya sebesar |a - b|.

Pak Bos diberikan sebuah matrix `S`, bantulah ia mengonversi matrix tersebut menjadi sebuah <i>Magic Square</i> dengan biaya minimal dengan mengubah 0 atau lebih digitnya. Lalu print biaya tersebut pada sebuah baris baru.

<b>Note</b>: Hasil <i>Magic Square</i> harus mengandung integer berbeda dengan rentang [1, 9] (inclusive).

## Input Format
Terdapat 3 baris input. Tiap baris mendeskripsikan baris dari matrix dengan format 3 space-separated integers menandakan elemen ke 1, 2, dan 3 dari baris tersebut.

## Output Format
Print sebuah integer yang menandakan jumlah biaya minimum untuk mengubah matrix `S` menjadi sebuah <i>Magic Square</i>

## Sample Input
````
4 9 2
3 5 7
8 1 5
````

## Sample Ouptut
````
1
````

## Penjelasan
Matrix `S`:
````
4 9 2
3 5 7
8 1 5
````
Matrix tersebut belum bisa disebut <i>Magic Square</i> karena tidak semua baris, kolom, maupun diagonal tengah nya jika dijumlahkan memiliki nilai yang sama.

Jika kita mengubah nilai kanan bawah `S`[2][2] dari 5 menjadi 6 dengan biaya |6 - 5| = 1, `S` menjadi <i>Magic Square</i> dengan biaya minimum yang mungkin. So, print 1 sebagai output dengan baris baru. (println)

## Constraint
- Matrix `S` terdiri dari integer berbeda dengan rentang [1, 9] inclusive.