import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i, j, k = 0, n = 0;
        int data[][] = new int[3][3];
        int hasil[] = new int[8];
        int arr[][][] = 
        {
            { 
                {4, 9, 2}, 
                {3, 5, 7}, 
                {8, 1, 6} 
            },
            { 
                {8, 3, 4}, 
                {1, 5, 9}, 
                {6, 7, 2} 
            },
            { 
                {6, 1, 8}, 
                {7, 5, 3}, 
                {2, 9, 4} 
            },
            { 
                {2, 7, 6}, 
                {9, 5, 1}, 
                {4, 3, 8} 
            },
            { 
                {2, 9, 4}, 
                {7, 5, 3}, 
                {6, 1, 8} 
            },
            { 
                {6, 7, 2}, 
                {1, 5, 9}, 
                {8, 3, 4} 
            },
            { 
                {8, 1, 6}, 
                {3, 5, 7}, 
                {4, 9, 2} 
            },
            { 
                {4, 3, 8}, 
                {9, 5, 1}, 
                {2, 7, 6} 
            }
        };
        
        for (i = 0; i < 3; i++) for (j = 0; j < 3; j++) data[i][j] = in.nextInt();
        for (i = 0; i < 8; i++) for (j = 0; j < 3; j++) for (k = 0; k < 3; k++) hasil[i] += Math.abs(arr[i][j][k] - data[j][k]);
        System.out.println(min(hasil));
    }

    public static void trace(Object... args) {
        for (int i = 0; i < args.length; i++) System.out.print(args[i] + " ");
        System.out.println();
    }

    public static int min(int arr[]) {
        int i, min = arr[0];
        for (i = 1; i < arr.length; i++) if (arr[i] < min) min = arr[i];
        return min; 
    }
}