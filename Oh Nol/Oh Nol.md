## Description
Pak Ayyan sedang membersihkan rumahnya. Tiba-tiba saja iya menemukan secarik kertas berisi himpunan angka. Di belakangnya, tertulis sebuah kalimat "Jika kamu berhasil menemukan seluruh kombinasi 3 Angka yang berjumlah 0 dan mengirim jawabanmu ke email berikut, kamu akan memenangkan Uang Tunai Jutaan Rupiah". Karena Pak Ayyan sedang butuh uang, ia pun meluangkan waktunya untuk menyelesaikan permasalaah tersebut. Kamu sebagai anaknya jika berhasil membantunya dengan keahlian Skill Pemrogramanmu, kamu akan diberi sebagian dari Hadiah tersebut. Bantulah Pak Ayyan!


## Input Format
Satu baris berisi `n` buah angka

## Output Format

Jika tidak ada kombinasi yang mungkin, outputkan: `NO_KEY`

Jika ada, outputkan kombinasi angka yang dipisahkan dengan string ' | '


## Sample Input
```
1 2 3 4 5
```

## Sample Output
```
NO_KEY
```

## Explanation
Cetak NO_KEY karena tidak ada kombinasi yang mungkin


## Sample Input
```
-1 -2 -3 -4 -5 0 1 2 3 4 5
```

## Sample Output
```
-5 | 5 | 0
-5 | 4 | 1
-5 | 3 | 2
-4 | 5 | -1
-4 | 4 | 0
-4 | 3 | 1
-3 | 5 | -2
-3 | 4 | -1
-3 | 3 | 0
-3 | 2 | 1
-2 | 3 | -1
-2 | 2 | 0
-1 | 1 | 0
```

## Explanation
Cetak semua susunan kombinasi yang terurut dari elemen terkecil ke elemen terbesar.

## Constraints
- 0 < `n` ≤ 10<sup>7</sup>