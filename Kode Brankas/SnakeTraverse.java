import java.util.Scanner;
import java.util.ArrayList;
public class SnakeTraverse {
    public String snakeTraverse(int[][] mat) {
        String s = "";
        final String SEPARATOR = " - ";
        int len = mat.length;
        boolean topToBottom = false;
        for (int i = 0; i < (2 * mat.length - 1); i++) {
            if (i < len) {
                for (int j = 0; j <= i; j++) {
                    int x = j % len;
                    int y = (i % len) - x;
//                    System.out.println(x + " " + y);
                    if (topToBottom)
                        s += mat[x][y] + SEPARATOR;
                    else
                        s += mat [y][x] + SEPARATOR;
                }
            } else {
                // 4 -> 7
                for(int j = 1; j < (2 * len) - i; j++) {
                    // 0 -> 2, 0 -> 1, 0 -> 0
                    int x = i - len + j;
                    int y = i - x;
//                    System.out.println(x + " " + y);
                    if (topToBottom)
                        s += mat[x][y] + SEPARATOR;
                    else
                        s += mat [y][x] + SEPARATOR;
                }
            }

            topToBottom = !topToBottom;
        }
        return s.substring(0, s.length() - SEPARATOR.length());
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        SnakeTraverse app = new SnakeTraverse();
        /*
        input:
         T
         n <matrix 1 ukuran nxn>
         n <matrix 2 ukuran nxn>
         n <matrix 3 ukuran nxn>
         ...
         n <matrix T ukuran nxn

         contoh input:
         3 4 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 5 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 3 1 2 3 4 5 6 7 8 9
         --> 3 Test Case
         --> T1 -> n = 4
         --> T2 -> n = 5
         --> T3 -> n = 3
         */
//        int t = sc.nextInt();
//        while(t-- > 0) {
            int n = sc.nextInt();
            int mat[][] = new int[n][n];
            for(int i = 0; i < n; i++) {
                for(int j = 0; j < n; j++) {
                    mat[i][j] = sc.nextInt();
                }
            }
            System.out.println(app.snakeTraverse(mat));
//        }

//        int mat[][] = {
//                {11, 12, 13, 14},
//                {15, 16, 17, 18},
//                {19, 20, 21, 22},
//                {23, 24, 25, 26}
//        };
//        System.out.println(app.snakeTraverse(mat));
//        mat = new int[][]{
//                {11, 12, 13, 14, 15},
//                {16, 17, 18, 19, 20},
//                {21, 22, 23, 24, 25},
//                {26, 27, 28, 29, 30},
//                {31, 32, 33, 34, 35}
//        };
//        System.out.println(app.snakeTraverse(mat));
//        mat = new int[][]{
//                {1, 2, 3},
//                {4, 5, 6},
//                {7, 8, 9}
//        };
//        System.out.println(app.snakeTraverse(mat));
    }
}
