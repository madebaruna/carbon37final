## Description

Pak Ayyan yang merupakan saudara kandung dari Pak Aian memiliki sebuah brankas. Brankas tersebut didesain agar memiliki _layout_ tombol pengaman berupa matriks `n` x `n`.

Contoh _layout_ `n` = `3`:

```
1 2 3
4 5 6
7 8 9
```

Namun, agar tidak lupa, Pak Ayyan membuat sebuah algoritma zig-zag yang merupakan kunci dari brankas tersebut. Untuk ukuran `n` bernilai `3` (seperti _layout_ di atas), kuncinya ialah:

```
1 - 2 - 4 - 7 - 5 - 3 - 6 - 8 - 9
```

Baru-baru ini, ia menyimpan suatu barang yang tak ternilai harganya ke dalam brankas tersebut dan beliau ingin meningkatkan keamanan brankas tersebut dengan mengganti _layout_ tombol pengamannya dengan ukuran `n` yang lebih besar dan tombol yang diacak. Kamu sebagai orang yang sangat dipercaya Pak Ayyan diminta untuk membantunya mengatur kunci brankasnya yang baru sesuai algoritma yang telah Pak Ayyan tentukan. Bantulah Pak Ayyan!

Contoh _layout_ `n` = `4` dengan susunan tombol acak:
```
9  10 11 12
7  5  1  3
20 2  6  21
99 98 97 95
```

## Input Format

```
n
susunan layout tombol (matriks n*n)
```

## Output Format
```
urutan angka-angka kombinasi kunci brankas yang dipisahkan dengan ' - '
```

## Sample Input 1

```
4
9 10 11 12
7 5 1 3
20 2 6 21
99 98 97 95
```

## Sample Output 1

```
9 - 10 - 7 - 20 - 5 - 11 - 12 - 1 - 2 - 99 - 98 - 6 - 3 - 21 - 97 - 95
```

## Sample Input 2

### Note:

- Tombol pada brankas dapat berulang. Misal:

```
3
1 2 3
4 5 1
7 7 7
```

## Sample Output 2

```
1 - 2 - 4 - 7 - 5 - 3 - 1 - 7 - 7
```

## Constraints
- 1 ≤ n ≤ 10<sup>7</sup> 