
import java.util.Scanner;

public class AmirProblem {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int k = sc.nextInt();
    int prime[] = new int[n];
    prime[0] = 3;
    prime[1] = 5;
    int count = 2;
    for (int number = 7; number <= n && k != 0; number += 2) {
      int i = 0;
      while (i < count && number % prime[i] != 0) {
        ++i;
      }
      if (i == count) {
        for (int j = 1; j < count; ++j) {
          if (prime[j] + prime[j - 1] + 1 == number) {
            k -= 1;
            break;
          }
        }
        prime[count++] = number;
      }
    }
    System.out.println((k == 0)? "YA" : "TIDAK");

  }
}
