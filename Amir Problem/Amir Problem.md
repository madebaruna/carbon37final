## Description
Amir adalah seorang yang sangat tertarik pada bilangan prima, suatu 
hari ia membaca tentang Goldbach problem yang menyatakan bahwa setiap
bilangan genap yang lebih besar dari 2 dapat diekspresikan dari 
penjumlahan dua buah bilangan prima. Masalah tersebut menarik perhatian
amir sehingga dia memutuskan untuk membuat versinya sendiri yang disebut
amirPbach problem. karena amir hanya tertarik pada bilangan prima
amirPbach problem menyatakan bahwa setidaknya sebanyak k bilangan prima
dari 2 sampai n (inklusif) dapat diekspresikan dengan penjumlahan 3 angka
yaitu : 2 angka prima yang bersebelahan dan angka 1.
sebagai contoh, 19 = 7 + 11 + 1, atau 13 = 5 + 7 + 1   
  
Dua buah angka prima dikatakan bersebelahan jika tidak ada angka prima lain
diantaranya  
  
Dapatkah kamu membantu amir untuk membuktikan apakah dia benar atau salah
  
## Input
Dua buah integer n dan k (2 <= n  <= 100) and k (0 <= k <= 100).

## Output
Cetak "YA" jika paling tidak ada k bilangan prima dari 2 sampai n (inklusif)
yang bisa diekspresikan sebagaimana yang dijelaskan diatas, jika tidak
cetak "TIDAK" (Tanpa tanda kutip)

## Contoh Input 1
```
27 2
```
## Contoh Output 1
```
YA
```

## Contoh Input 2
45 7
## Contoh Output 2
TIDAK

### Note
Pada contoh yang pertama jawabannya YA karena paling sedikit ada 2 angka prima
yang bisa diekspresikan sebagaimana yang telah dijelaskan (yaitu 13 dan 19)
Pada contoh yang kedua jawabannya TIDAK karena tidak mungkin untuk membuktikan
7 angka dari 2 sampai 45 sesuai rumus yang sudah diberikan